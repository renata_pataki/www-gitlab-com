---
layout: markdown_page
title: "Category Direction - Navigation & Settings"
description: "This is the Product Direction Page for the Navigation and Settings patterns within GitLab."
canonical_path: "/direction/create/navigation_settings/"
---

- TOC
{:toc}

## Navigation & Settings

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

Thanks for visiting the GitLab Navigation & Settings category direction page. This page belongs to the [Editor group](/handbook/product/categories/#editor-group) of the Create stage and is maintained by Eric Schurter ([E-Mail](mailto:eschurter@gitlab.com)). Much of this page is inspired by the work our UX department has done over the years leading the way in research and design for the navigation and configuration experience within GitLab. As a global, foundational aspect of the GitLab product, this area does not have an obvious, single product owner. The Editor group is taking the reins to improve this critical aspect of the GitLab experience and increase GitLab's overall system usability score (SUS).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the issues linked in the [Settings](https://gitlab.com/groups/gitlab-org/-/epics/4410) and [Navigation](https://gitlab.com/groups/gitlab-org/-/epics/4413) epics. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of navigation issues or ideas for enhancements, we'd especially love to hear from you.

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

We are responsible for creating navigational structures that are intuitive, in tune with user needs, and representative of the numerous workflows of our community of users. However, when designing for the needs of so many different people, we often have to make compromises and not everyone is pleased with the result. Navigation is not just about getting from point A to B; it can shape workflows, empower users to discover new, more efficient ways of working, and ultimately determine how comfortable users are with a product. From the moment users log in for the first time to when they start diving deeper into GitLab’s diverse feature set, our navigation structure is critical for shaping the user's path and, ultimately, their success in using GitLab.

Similarly, GitLab can be customized and configured to meet the specific needs of a wide variety of use cases. The flexibility offered within each feature is exposed in the Settings pages and the overall experience of GitLab is greatly impacted by the user's ability to discover, manipulate, and return to these settings with confidence and consistency.

- An overview of Navigation can be found [here](https://about.gitlab.com/handbook/engineering/ux/navigation/)
- We also have [design system documentation](https://design.gitlab.com/regions/navigation) related to navigation.

### Where we are Headed
<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->

We want to provide a better experience for users to complete common navigational tasks. These include:

  - staying up to date on their important projects
  - seeing new comments and work activity by team members
  - getting back to a known issue, MR, project or other work object
  - seeing status of items they are responsible for

Navigation is currently project and group centric. It’s easy enough to find “Project Things” and “Group Things” such as a project issue list, but it’s harder for an individual to find “My Things”, such as an aggregate of their MR pipelines.  We don’t provide users with tools to quickly get to items that are important to them. This is where we are headed.

#### Our current navigation is centered around `projects` and `groups`.


***Projects***
``` mermaid
graph TD
  Project-->Project_MRs
  Project-->Project_Issues
  Project-->Project_Pipelines
  Project-->Project_Files
  Project-->Project_Activity
```
***Groups***
``` mermaid
graph TD
  Group-->Group_Members
  Group-->Group_Epics
  Group-->Group_MRs
  Group-->Group_Issues
  Group-->Group_Activity
```
#### In the future we need to also enable a `user centric` navigation that feels more like this.

***Users***
``` mermaid
graph TD
  Me-->Things_I_watch
  Me-->Issues_I_create
  Me-->MRs_I_contribute_to
  Me-->Pipelines_for_MRs_or_projects_I_work_on
  Me-->People_I_follow
```

With regard to the Settings experience, we are looking to improve discoverability and consistency within the pages. Our efforts are focused on:

  - improving discoverability of existing settings
  - incorporating search within the settings experience
  - consistently apply UI patterns and re-align the information architecture within settings pages
  - provide quick access to settings without removing the user from their current context

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category. An overview
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->

In short, all [roles & personas](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/) interact with navigation. We are focusing on helping users orient themselves around the things that are most important to them, so they can be more productive.

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

#### Areas of focus:
1. [Global and Contextual Navigation](https://gitlab.com/groups/gitlab-org/-/epics/4413)
1. [GitLab Settings](https://gitlab.com/groups/gitlab-org/-/epics/4410)
1. Other UI features that aid in discoverability from a browse and discover standpoint (this excludes search, which is about locating a known object)

### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

We have not planned to launch a whole new navigation experience. The work we have in the backlog to enhance the navigation is rather high in volume and many of the issues are heavy in weight. At this time we are focusing on issues that we can ship quickly that will have a high impact on how users interact with Gitlab.

### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

This category is currently at the "Viable" maturity level, and our next maturity target is "Complete" by the the end of Q1 2021 (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).


### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Recently, we conducted system usability research to better understand how our users interact with GitLab and also benchmark our results. In the feedback from the open text of the survey, we found a few key themes that tie back to our navigation. Below is a list of videos explaining our findings.

- [SUS Actionalble Inights](https://www.youtube.com/playlist?list=PL05JrBw4t0KrPNM6WFlrsVM8tWwkYDyio)
- [Left-side navigation](https://www.youtube.com/watch?v=ZeCdGTe_hbU&list=PL05JrBw4t0KrPNM6WFlrsVM8tWwkYDyio&index=2&t=0s)
- [UI Polish](https://www.youtube.com/watch?v=yLaqC_hb9vs&list=PL05JrBw4t0KrPNM6WFlrsVM8tWwkYDyio&index=3&t=0s)

### Top internal customer issue(s) and epic(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

- [GitLab Navigation Prioritization & Scope](https://gitlab.com/groups/gitlab-org/-/epics/4413)
- [GitLab Settings Prioritization & Scope](https://gitlab.com/groups/gitlab-org/-/epics/4410)
- [System Usability Key Theme - Navigation and Discoverability](https://gitlab.com/groups/gitlab-org/-/epics/3848)


### Other Important Links
<!-- What's the most important thing to move your strategy forward?-->

- [https://about.gitlab.com/handbook/engineering/ux/navigation/](https://about.gitlab.com/handbook/engineering/ux/navigation/)
- [https://about.gitlab.com/blog/2019/07/31/navigation-state-of-play/](https://about.gitlab.com/blog/2019/07/31/navigation-state-of-play/)
- [https://about.gitlab.com/direction/expansion/](https://about.gitlab.com/direction/expansion/)
- [https://about.gitlab.com/handbook/engineering/development/growth/expansion/](https://about.gitlab.com/handbook/engineering/development/growth/expansion/)
