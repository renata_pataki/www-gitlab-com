require 'set'

module Gitlab
  module Homepage
    class OrgChart
      attr_reader :nodes, :reports

      Node = Struct.new(:member, :reports) do
        def total_count
          @total_count ||= reports.map(&:total_count).sum + direct_count
        end

        def direct_count
          @direct_count ||= reports.count { |r| !r.vacancy? }
        end

        def vacancies
          @vacancies ||= reports.map(&:vacancies).sum + reports.count(&:vacancy?)
        end

        def to_list
          [self] + reports.flat_map(&:to_list)
        end

        def vacancy?
          member['type'] == 'vacancy'
        end
      end

      def initialize(team)
        @nodes = team.to_h { |person| [person['slug'], person] }
        @reports = team.group_by { |person| person['reports_to'] }
      end

      def validate
        @validate ||= [verify_non_empty!, verify_no_missing_managers!, verify_no_cycles!].compact
      end

      def validate!
        raise 'Invalid' unless validate.empty?
      end

      def forest
        validate! # only valid to call after validation

        @forest ||= tree({ 'slug' => nil }).reports
      end

      private

      def tree(person)
        nodes = reports.fetch(person['slug'], []).map { |p| tree(p) }
        nodes = nodes.sort_by { |n| [-n.total_count, n.member['slug']] }
        Node.new(person, nodes)
      end

      def verify_non_empty!
        return if nodes.any?

        "Team is empty"
      end

      def verify_no_missing_managers!
        missing_managers = reports.keys.compact - nodes.keys
        orphans = missing_managers.flat_map do |manager|
          reports[manager].map { |p| "#{manager}: #{p['slug']} (#{p['name']})" }
        end

        return if missing_managers.none?

        <<~MSG
          Missing managers!
          ---------------------

          The following managers are missing:
          #{missing_managers.map { |slug| " - #{slug}" }.join("\n")}

          This could be a typo in a `reports_to` line, or perhaps
          the manager was removed and the reports haven't been
          updated?

          The reports of these managers are:
          #{orphans.map { |str| " - #{str}" }.join("\n")}
        MSG
      end

      def verify_no_cycles!
        return unless cycle

        <<~MSG
          Cycle found in org-chart!
          -------------------------

          The org-chart should not have any cycles. We found the following cycle:

          #{cycle.join(' --[reports_to]--> ')}
        MSG
      end

      def cycle
        return @cycle if defined? @cycle

        @cycle = begin
          seen = Set.new
          nodes.values.each do |node|
            next if seen.include?(node['slug'])

            (kind, path) = path_to_root(node)
            path.each { |slug| seen << slug }

            return path if kind == :cycle
          end

          nil
        end
      end

      def path_to_root(node)
        path = [node['slug']]
        curr = node
        while manager_slug = curr['reports_to']
          manager = nodes[manager_slug]
          return [:missing_manager, path] if manager.nil? # see verify_no_missing_managers!
          return [:cycle, path] if path.include?(manager_slug)

          path.push(manager_slug)
          curr = manager
        end

        [:linear, path]
      end
    end
  end
end
