---
layout: handbook-page-toc
title: Fulfillment Sub-department
description: "The Fulfillment Sub-department is composed of development teams, Purchase, License, and Utilization, working on the infrastructure between the systems which affect the user purchasing process that support the GitLab DevOps Platform."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Welcome to the Fulfillment Sub-department Page

### Who are we and what do we work on?

The Fulfillment Sub-department is responsible for the infrastructure between the systems which affect the user purchasing process, as well as being the primary caretakers of the [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) and [LicenseDot](/handbook/engineering/development/fulfillment/architecture/#licensedot) systems.

We frequently collaborate with other teams. For example if we are making a change which will affect [usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#usage-ping) we collaborate with the [Product Intelligence group](/handbook/product/categories/#product-intelligence-group). When our work involves or affects any backend enterprise application, we collaborate with the [Sales Systems team](/handbook/sales/field-operations/sales-systems/).

### FY22 Direction

The Fulfillment Engineering team has grown substantially with an influx of new staff through hiring and internal realignments over the previous year.  Our success in FY22 will be determined by how well we can up-skill and motivate these new team members along with the relationships and trust we build with our stable counterparts in Product, UX and Quality while we work to deliver our team's roadmap.

In order to support GitLab's continued business growth, the applications we are responsible for need to be able to scale while reducing our customer’s needs for manual support intervention.  To achieve this, our Engineering efforts and product roadmap are concentrated around ensuring our Fulfillment process:

* Is a delightful, performant and error free purchasing and licensing experience for our customers.
* Provides our customers the tools they need to manage their accounts and licenses.
* Improves and automates more of the complexity of our sales process in order to reduce processing and intervention by our sales teams.

We measure our success by our [performance indicators](https://about.gitlab.com/handbook/engineering/development/performance-indicators/fulfillment/) and our team members engagement and wellbeing.  We support our individual team members by having regular career conversations, [gathering actionable feedback](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10558) on ways we can improve the way we work, and reinforcing our [Diversity, Inclusion and Belonging](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10627) value through ongoing training.  We aim to run quarterly team days to help build relationships and foster collaboration.

### Groups

#### License
Responsible for retrieving and managing licenses, overall system architecture, data integrity. Systems: [LicenseDot](/handbook/engineering/development/fulfillment/architecture/#licensedot). Integration: Salesforce.

#### Purchase
Responsible for all purchase experiences. Trials, purchase flows, and account management (invoices, contact, billing info) Systems: [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot), [GitLab](/handbook/engineering/development/fulfillment/architecture/#gitlab). Integrations: [Zuora](/handbook/engineering/development/fulfillment/architecture/#zuora), [Stripe](/handbook/engineering/development/fulfillment/architecture/#stripe).

#### Utilization
Responsible for all consumables management, usage reporting, and usage notifications (excluding license utilization). Systems: [GitLab](/handbook/engineering/development/fulfillment/architecture/#gitlab).




## Want to know more about us?

* [Product Vision](https://about.gitlab.com/direction/fulfillment/)
* [Fulfillment section](/handbook/product/product-categories/#fulfillment-section)
* Engineering Teams
     * Purchase
          * [Frontend](/handbook/engineering/development/fulfillment/fe-purchase/)
          * [Backend](/handbook/engineering/development/fulfillment/be-purchase/)
     * License
          * [Backend](/handbook/engineering/development/fulfillment/be-license/)
     * Utilization
          * [Backend](/handbook/engineering/development/fulfillment/be-utilization/)
* [Fulfillment Software Architecture](/handbook/engineering/development/fulfillment/architecture)
* [Fulfillment Resources](/handbook/engineering/development/fulfillment/resources)


## What are we working on?
* [FY22 Q2 Quad OKR's](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/169) (In Progress)
* [FY22 Q1 Engineering OKR's](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10680)

### Purchase

* [13.11 Board](https://gitlab.com/groups/gitlab-org/-/boards/2471198?milestone_title=13.11&&label_name%5B%5D=group%3A%3Apurchase)
* [13.11 Frontend Board](https://gitlab.com/groups/gitlab-org/-/boards/2341677?&label_name%5B%5D=devops%3A%3Afulfillment&label_name%5B%5D=frontend)
* [13.11 Planning Issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/168)

### License

* [13.11 Board](https://gitlab.com/groups/gitlab-org/-/boards/2474045?milestone_title=13.11&&label_name%5B%5D=group%3A%3Alicense)
* [13.11 Planning Issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/183)

### Utilization

* [13.11 Board](https://gitlab.com/groups/gitlab-org/-/boards/2473827?milestone_title=13.11&&label_name%5B%5D=group%3A%3Autilization)
* [13.11 Planning Issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/187)

## How we work

* In accordance with our [GitLab values](/handbook/values/)
* Transparently: nearly everything is public, we record/livestream meetings whenever possible
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

### Planning

We plan in monthly cycles in accordance with our [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline).
Release scope for an upcoming release should be finalized by the `1st`.

On or around the `26th`: Product meets with Engineering Managers for a preliminary issue review. Issues are tagged with a milestone and are estimated initially.

### Estimation

Before work can begin on an issue, we should estimate it first after a preliminary investigation. This is normally done in the monthly planning meeting.

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements.
| 13| A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues.

In planning and estimation, we value [velocity over predictability](/handbook/engineering/#velocity-over-predictability). The main goal of our planning and estimation is to focus on the [MVC](/handbook/values/#minimal-viable-change-mvc), uncover blind spots, and help us achieve a baseline level of predictability without over optimizing. We aim for 70% predictability instead of 90%. We believe that optimizing for velocity (MR throughput) enables our Fulfillment teams to achieve a [weekly experimentation cadence](/handbook/product/growth/#weekly-growth-meeting).

- If an issue has many unknowns where it's unclear if it's a 1 or a 5, we will be cautious and estimate high (5).
- If an issue has many unknowns, we can break it into two issues. The first issue is for research, also referred to as a [Spike](https://en.wikipedia.org/wiki/Spike_(software_development)), where we de-risk the unknowns and explore potential solutions. The second issue is for the implementation.
- If an initial estimate is incorrect and needs to be adjusted, we revise the estimate immediately and inform the Product Manager. The Product Manager and team will decide if a milestone commitment needs to be adjusted.

#### Estimation Template

The following is a guiding mental framework for engineers to consider when contributing to estimates on issues.

```
### Refinement / Weighting

**Ready for Development**: Yes/No

<!--
Yes/No

Is this issue sufficiently small enough, or could it be broken into smaller issues? If so, recommend how the issue could be broken up.

Is the issue clear and easy to understand?
-->

**Weight**: X

**Reasoning**:

<!--
Add some initial thoughts on how you might breakdown this issue. A bulleted list is fine.

This will likely require the code changes similar to the following:

- replace the hexdriver with a sonic screwdriver
- rewrite backups to magnetic tape
- send up semaphore flags to warn others

Links to previous example. Discussions on prior art. Notice examples of the simplicity/complexity in the proposed designs.
-->


**MR Count**: Y
<!--
- 1 MR to update the driver worker
- 1 MR to update docs regarding mag tape backups

Let me draw your attention to potential caveats.
-->

**Testing considerations**:
<!--
- ensure that rotation speed of sonic screwdriver doesn't exceed rotational limits
-->
```

### Picking something to work on

Engineers can find and open [the milestone board for Fulfillment](https://gitlab.com/groups/gitlab-org/-/boards/)
and begin working first on those with the `deliverable` label.

It's possible for engineers to pick any of the remaining issues for the milestone once the deliverables are done. If the engineer has no preference, they can choose the next available issue from the top.

The following table will be used as a guideline for scheduling work within the milestone:

| Type        | % of Milestone | Description                                                                                |
|-------------|----------------|-------------|------------------------------------------------------------------------------|
| Deliverable | 40%            | business priorities (compliance, IACV, efficiency initiatives)                             |
| Bug         | 16%            | non-critical bug fixes                                                                     |
| Tech debt   | 10%            |                                                                                            |
| Features    | 14%            | non-critical                                                                               |
| Other       | 20%            | engineer picks, critical security/data/availability/regression, urgent business priorities |

#### Organizing the work

We generally follow the [Product Development Flow](/handbook/product-development-flow/#workflow-summary):
1. `workflow::problem validation` - needs clarity on the problem to solve
1. `workflow::design` - needs a clear proposal (and mockups for any visual aspects)
1. `workflow::solution validation` - designs need to be evaluated by customers, and/or other GitLab team members for usability and feasibility
1. `workflow::planning breakdown` - needs a Weight estimate
1. `workflow::scheduling` - needs a milestone assignment
1. `workflow::ready for development`
1. `workflow::in dev`
1. `workflow::in review`
1. `workflow::verification` - code is in production and pending verification by the DRI engineer

Generally speaking, issues are in one of two states:
* Discovery/refinement: we're still answering questions that prevent us from starting development,
* Implementation: an issue is waiting for an engineer to work on it, or is actively being built.

Basecamp thinks about these stages in relation to the [climb and descent of a hill](https://www.feltpresence.com/hills.html).

While individual groups are free to use as many stages in the [Product Development Flow](/handbook/product-development-flow/#workflow-summary) workflow as they find useful, we should be somewhat prescriptive on how issues transition from discovery/refinement to implementation.

### Weekly async issue updates

Every Friday, each engineer is expected to provide a quick async issue update by commenting on their assigned issues using the following template:

```
<!---
Please be sure to update the workflow labels of your issue to one of the following (that best describes the status)"
- ~"workflow::In dev"
- ~"workflow::In review"
- ~"workflow::verification"
- ~"workflow::blocked"
-->
### Async issue update
1. Please provide a quick summary of the current status (one sentence).
1. When do you predict this feature to be ready for maintainer review?
1. Are there any opportunities to further break the issue or merge request into smaller pieces (if applicable)?
1. Were expectations met from a previous update? If not, please explain why.
```

We do this to encourage our team to be more async in collaboration and to allow the community and other team members to know the progress of issues that we are actively working on.

### Quality

#### Quad-planning workflow

When a `feature` or `Deliverable` proposal issue moves into `workflow::planning breakdown`, [SETs](/handbook/engineering/quality/#stable-counterparts) owns the completion of the `Availability and Testing` section in the Feature Proposal to complete the definition of done. As we grow to reach our [desired ratio](/handbook/engineering/quality/#staffing-planning), we will only have the quad approach in groups where we have an assigned SET in place.
1. PM applies the label `quad-planning::ready` when the feature is reviewed by the team and is ready to be implemented.
1. SET is the DRI for the `Availability and Testing` section, ensuring that the strategy accounts for all test levels and facilitating discussions and feedback with the group.
1. SET determines what regressions will need to be run, this is made clear in the above section.
1. SET applies the label `quad-planning::complete-action` if they have are recommendations (e.g. running regression job, writing additional tests, etc.).
1. SET applies the label `quad-planning::complete-no-action` if there is no additional actions needed.

[Quad Planning Dashboard](https://app.periscopedata.com/app/gitlab/634305/Quad-Planning-Dashboard?widget=8844548&udv=1040834) showcases the total Planned issues for Quad Planning vs the actual ones for each milestone.

#### Tests

The CustomersDot has different types of tests running:

1. Linting and [rubocop](https://github.com/rubocop-hq/rubocop) jobs
1. Unit tests (specs, these could be of many types, such as controller specs)
1. Integration tests (specs, mocking external calls)
1. Frontend tests
1. E2E integration tests (TBD)

We also have a flag `VCR` that mocks external calls to Zuora by default. We have a [daily pipeline](https://gitlab.com/gitlab-org/customers-gitlab-com/pipeline_schedules) that runs at 9AM UTC with the flag set so the API calls hit the Zuora sandbox and we are notified of any failure (due to potential API changes).

Any test failure is notified to #s_fulfillment_status including a link to the pipeline. Pipeline failures will prevent deployments to staging and production.

### Deployment

#### CustomersDot

We use CD (Continuous Deployment) for [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/) and a MR goes through the following stages once it gets merged into the `staging` branch:

```mermaid
graph TD;
    A(Merged) --> |Green tests| B(Staging);
    B --> C[E2E Test on Staging in 40 minutes];
    C --> D[Verification]
    D --> E(Auto deploy to production in 2 hours);
```

If something goes wrong at the `Verification` stage, we could create an issue with the label `production::blocker`, which will prevent deployment to production. The issue cannot be confidential.

For MRs with significant changes, we should consider using [feature flags](https://gitlab.com/gitlab-org/customers-gitlab-com/#feature-flags-unleash) or create an issue with the `production::blocker` label to pause deployment and a allow for longer testing.


#### LicenseDot

We use an [automatic deployment to staging, but manual deployment to production](https://docs.gitlab.com/ee/topics/autodevops/#deployment-strategy) for LicenseDot.

Maintainers of the application need to trigger a manual action on the `master` branch in order to deploy to production.

The app lives at https://license.gitlab.com

The `staging` environment can be found [here](https://gitlab-org-license-gitlab-com-staging.license-stg.gitlab.org)

#### Escalation Process

In most cases an MR should follow the standard process of review, maintainer review, merge, and deployment as outlined above. When production is broken:

1. First determine whether the [Rapid Engineering Response](/handbook/engineering/workflow/#rapid-engineering-response) process can be followed. This will depend on availability and how critical the situation is.
1. The three hour wait between Staging and Production auto deploy can be bypassed with a manual deployment by a Maintainer.
1. When there are no project maintainers available ([CustomersDot](/handbook/engineering/projects/#customers-app), [LicenseDot](/handbook/engineering/projects/#license-app)) an additional GitLab Team Member with Maintainer access can be asked to assist.

In these cases please ensure:

1. There is an issue describing the reasons for the escalation, as per the [Rapid Engineering Response](/handbook/engineering/workflow/#rapid-engineering-response). Consider '@'mentioning the relevant [Growth/Fulfillment teams](https://gitlab.com/gitlab-org/growth).
1. The change is announced on the [#s_fulfillment](https://gitlab.slack.com/archives/CMJ8JR0RH) channel.

#### Feature freeze

The feature freeze for Fulfillment occurs at the same time as the rest of the company, normally around the 18th.

| App | Feature freeze (*) | Milestone ends |
| ---      |  ------  |----------|
| GitLab.com   | ~18th-22nd   | Same as the freeze |
| Customers/License   | ~18th-22nd   | Same as the freeze |

(*) feature freeze may vary according to the [auto-deploy document](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/auto-deploy.md).

Any issues not merged on the current milestone post feature freeze, will need to be moved to the next one (priority may also change for those).

### Measuring Engineering Throughput

One of our main engineering metrics is [throughput](/handbook/engineering/management/throughput/) which is the total number of MRs that are completed and in production in a given period of time. We use throughput to encourage small MRs and to practice our values of [iteration](/handbook/values/#iteration). Read more about [why we adoped this model](/handbook/engineering/management/throughput/#why-we-adopted-this-model).

[Fulfillment Section Performance Indicators](https://about.gitlab.com/handbook/engineering/development/performance-indicators/fulfillment/)

### CustomersDot Engineer:Maintainer ratio

<embed width="<%= 99 %>%" height="350" src="<%= signed_periscope_url(dashboard: 825250, chart: 11035807, embed: 'v2') %>">

### Meetings (Sync)

We hold optional synchronous social meetings weekly where we chat about anything outside work.
These meetings happen each Wednesday alternating between 10:00am UTC (EMEA) and 4:00pm (AMER).
Check the [Fulfillment Google calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7199q584haas4tgeuk9qnd48nc%40group.calendar.google.com&ctz=America%2FBogota) for more information.

### Retrospectives

After the `8th`, the Fulfillment team conducts an [asynchronous retrospective](/handbook/engineering/management/team-retrospectives/). You can find current and past retrospectives for Fulfillment in [https://gitlab.com/gl-retrospectives/fulfillment/issues/](https://gitlab.com/gl-retrospectives/fulfillment/issues/).

## How to get help

* For help with a license error, resending a license or other support requests, [create an internal issue for the support team](/handbook/support/internal-support).
* For general product questions, try the [#Questions](https://gitlab.slack.com/archives/C0AR2KW4B) Slack channel.
* Reach out to [#s_fulfillment](https://gitlab.slack.com/archives/CMJ8JR0RH) with non-customer specific purchasing or provisioning questions, or to escalate an outage in our purchasing workflow.
* To raise awareness or request elevated priority of an issue, mention [@amandarueda](https://gitlab.com/amandarueda) in the issue and provide relevant information.

## Common links

 * [All open Fulfillment epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Afulfillment)
 * [Issue Tracker](https://gitlab.com/gitlab-org/fulfillment-meta/issues)
 * [Slack channel #s_fulfillment](https://gitlab.slack.com/app_redirect?channel=s_fulfillment)
 * [Daily standup channel #s_fulfillment_daily](https://gitlab.slack.com/app_redirect?channel=s_fulfillment_daily)
 * [Team calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7199q584haas4tgeuk9qnd48nc%40group.calendar.google.com)
 * [Fulfillment technical debt status](https://app.periscopedata.com/app/gitlab/618368/Growth-technical-debt-status)
