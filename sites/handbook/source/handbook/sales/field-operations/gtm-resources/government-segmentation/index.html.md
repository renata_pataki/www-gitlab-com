---
layout: handbook-page-toc
title: "FY22 Population for Government Segmentation"
description: "THIS PAGE IS OUTDATED - DO NOT USE"
---

**THIS PAGE IS OUTDATED - DO NOT USE**

Country population as used to determine FY22 Government Segmentation. This data is frozen for FY22 account planning and will next be reviewed for FY23 planning and segmentation.

| Country (or dependency) | Population (2020) | Segment |
| ---  | ---  | --- |
| China | 1,439,323,776 | Large |
| India | 1,380,004,385 | Large |
| United States | 331,002,651 | Large |
| Indonesia | 273,523,615 | Large |
| Pakistan | 220,892,340 | Large |
| Brazil | 212,559,417 | Large |
| Nigeria | 206,139,589 | Large |
| Bangladesh | 164,689,383 | Large |
| Russia | 145,934,462 | Large |
| Mexico | 128,932,753 | Large |
| Japan | 126,476,461 | Large |
| Ethiopia | 114,963,588 | Large |
| Philippines | 109,581,078 | Large |
| Egypt | 102,334,404 | Large |
| Vietnam | 97,338,579 | Large |
| DR Congo | 89,561,403 | Large |
| Turkey | 84,339,067 | Large |
| Iran | 83,992,949 | Large |
| Germany | 83,783,942 | Large |
| Thailand | 69,799,978 | Large |
| United Kingdom | 67,886,011 | Large |
| France | 65,273,511 | Large |
| Italy | 60,461,826 | Large |
| Tanzania | 59,734,218 | Large |
| South Africa | 59,308,690 | Large |
| Myanmar | 54,409,800 | Large |
| Kenya | 53,771,296 | Large |
| South Korea | 51,269,185 | Large |
| Colombia | 50,882,891 | Large |
| Spain | 46,754,778 | Large |
| Uganda | 45,741,007 | Large |
| Argentina | 45,195,774 | Large |
| Algeria | 43,851,044 | Large |
| Sudan | 43,849,260 | Large |
| Ukraine | 43,733,762 | Large |
| Iraq | 40,222,493 | Large |
| Afghanistan | 38,928,346 | Large |
| Poland | 37,846,611 | Large |
| Canada | 37,742,154 | Large |
| Morocco | 36,910,560 | Large |
| Saudi Arabia | 34,813,871 | Large |
| Uzbekistan | 33,469,203 | Large |
| Peru | 32,971,854 | Large |
| Angola | 32,866,272 | Large |
| Malaysia | 32,365,999 | Large |
| Mozambique | 31,255,435 | Large |
| Ghana | 31,072,940 | Large |
| Yemen | 29,825,964 | Large |
| Nepal | 29,136,808 | Large |
| Venezuela | 28,435,940 | Large |
| Madagascar | 27,691,018 | Large |
| Cameroon | 26,545,863 | Large |
| Côte d'Ivoire | 26,378,274 | Large |
| North Korea | 25,778,816 | Large |
| Australia | 25,499,884 | Large |
| Niger | 24,206,644 | Large |
| Taiwan | 23,816,775 | Large |
| Sri Lanka | 21,413,249 | Large |
| Burkina Faso | 20,903,273 | Large |
| Mali | 20,250,833 | Large |
| Romania | 19,237,691 | Commercial |
| Malawi | 19,129,952 | Commercial |
| Chile | 19,116,201 | Commercial |
| Kazakhstan | 18,776,707 | Commercial |
| Zambia | 18,383,955 | Commercial |
| Guatemala | 17,915,568 | Commercial |
| Ecuador | 17,643,054 | Commercial |
| Syria | 17,500,658 | Commercial |
| Netherlands | 17,134,872 | Commercial |
| Senegal | 16,743,927 | Commercial |
| Cambodia | 16,718,965 | Commercial |
| Chad | 16,425,864 | Commercial |
| Somalia | 15,893,222 | Commercial |
| Zimbabwe | 14,862,924 | Commercial |
| Guinea | 13,132,795 | Commercial |
| Rwanda | 12,952,218 | Commercial |
| Benin | 12,123,200 | Commercial |
| Burundi | 11,890,784 | Commercial |
| Tunisia | 11,818,619 | Commercial |
| Bolivia | 11,673,021 | Commercial |
| Belgium | 11,589,623 | Commercial |
| Haiti | 11,402,528 | Commercial |
| Cuba | 11,326,616 | Commercial |
| South Sudan | 11,193,725 | Commercial |
| Dominican Republic | 10,847,910 | Commercial |
| Czech Republic (Czechia) | 10,708,981 | Commercial |
| Greece | 10,423,054 | Commercial |
| Jordan | 10,203,134 | Commercial |
| Portugal | 10,196,709 | Commercial |
| Azerbaijan | 10,139,177 | Commercial |
| Sweden | 10,099,265 | Commercial |
| Honduras | 9,904,607 | Commercial |
| United Arab Emirates | 9,890,402 | Commercial |
| Hungary | 9,660,351 | Commercial |
| Tajikistan | 9,537,645 | Commercial |
| Belarus | 9,449,323 | Commercial |
| Austria | 9,006,398 | Commercial |
| Papua New Guinea | 8,947,024 | Commercial |
| Serbia | 8,737,371 | Commercial |
| Israel | 8,655,535 | Commercial |
| Switzerland | 8,654,622 | Commercial |
| Togo | 8,278,724 | Commercial |
| Sierra Leone | 7,976,983 | Commercial |
| Hong Kong | 7,496,981 | Commercial |
| Laos | 7,275,560 | Commercial |
| Paraguay | 7,132,538 | Commercial |
| Bulgaria | 6,948,445 | Commercial |
| Libya | 6,871,292 | Commercial |
| Lebanon | 6,825,445 | Commercial |
| Nicaragua | 6,624,554 | Commercial |
| Kyrgyzstan | 6,524,195 | Commercial |
| El Salvador | 6,486,205 | Commercial |
| Turkmenistan | 6,031,200 | Commercial |
| Singapore | 5,850,342 | Exception - Large |
| Denmark | 5,792,202 | Commercial |
| Finland | 5,540,720 | Commercial |
| Congo | 5,518,087 | Commercial |
| Slovakia | 5,459,642 | Commercial |
| Norway | 5,421,241 | Commercial |
| Oman | 5,106,626 | Commercial |
| State of Palestine | 5,101,414 | Commercial |
| Costa Rica | 5,094,118 | Commercial |
| Liberia | 5,057,681 | Commercial |
| Ireland | 4,937,786 | Commercial |
| Central African Republic | 4,829,767 | Commercial |
| New Zealand | 4,822,233 | Exception - Large |
| Mauritania | 4,649,658 | Commercial |
| Panama | 4,314,767 | Commercial |
| Kuwait | 4,270,571 | Commercial |
| Croatia | 4,105,267 | Commercial |
| Moldova | 4,033,963 | Commercial |
| Georgia | 3,989,167 | Commercial |
| Eritrea | 3,546,421 | Commercial |
| Uruguay | 3,473,730 | Commercial |
| Bosnia and Herzegovina | 3,280,819 | Commercial |
| Mongolia | 3,278,290 | Commercial |
| Armenia | 2,963,243 | Commercial |
| Jamaica | 2,961,167 | Commercial |
| Qatar | 2,881,053 | Commercial |
| Albania | 2,877,797 | Commercial |
| Puerto Rico | 2,860,853 | Commercial |
| Lithuania | 2,722,289 | Commercial |
| Namibia | 2,540,905 | Commercial |
| Gambia | 2,416,668 | Commercial |
| Botswana | 2,351,627 | Commercial |
| Gabon | 2,225,734 | Commercial |
| Lesotho | 2,142,249 | Commercial |
| North Macedonia | 2,083,374 | Commercial |
| Slovenia | 2,078,938 | Commercial |
| Guinea-Bissau | 1,968,001 | Commercial |
| Latvia | 1,886,198 | Commercial |
| Bahrain | 1,701,575 | Commercial |
| Equatorial Guinea | 1,402,985 | Commercial |
| Trinidad and Tobago | 1,399,488 | Commercial |
| Estonia | 1,326,535 | Commercial |
| Timor-Leste | 1,318,445 | Commercial |
| Mauritius | 1,271,768 | Commercial |
| Cyprus | 1,207,359 | Commercial |
| Eswatini | 1,160,164 | Commercial |
| Djibouti | 988,000 | Commercial |
| Fiji | 896,445 | Commercial |
| Réunion | 895,312 | Commercial |
| Comoros | 869,601 | Commercial |
| Guyana | 786,552 | Commercial |
| Bhutan | 771,608 | Commercial |
| Solomon Islands | 686,884 | Commercial |
| Macao | 649,335 | Commercial |
| Montenegro | 628,066 | Commercial |
| Luxembourg | 625,978 | Commercial |
| Western Sahara | 597,339 | Commercial |
| Suriname | 586,632 | Commercial |
| Cabo Verde | 555,987 | Commercial |
| Micronesia | 548,914 | Commercial |
| Maldives | 540,544 | Commercial |
| Malta | 441,543 | Commercial |
| Brunei | 437,479 | Commercial |
| Guadeloupe | 400,124 | Commercial |
| Belize | 397,628 | Commercial |
| Bahamas | 393,244 | Commercial |
| Martinique | 375,265 | Commercial |
| Iceland | 341,243 | Commercial |
| Vanuatu | 307,145 | Commercial |
| French Guiana | 298,682 | Commercial |
| Barbados | 287,375 | Commercial |
| New Caledonia | 285,498 | Commercial |
| French Polynesia | 280,908 | Commercial |
| Mayotte | 272,815 | Commercial |
| Sao Tome & Principe | 219,159 | Commercial |
| Samoa | 198,414 | Commercial |
| Saint Lucia | 183,627 | Commercial |
| Channel Islands | 173,863 | Commercial |
| Guam | 168,775 | Commercial |
| Curaçao | 164,093 | Commercial |
| Kiribati | 119,449 | Commercial |
| Grenada | 112,523 | Commercial |
| St. Vincent & Grenadines | 110,940 | Commercial |
| Aruba | 106,766 | Commercial |
| Tonga | 105,695 | Commercial |
| U.S. Virgin Islands | 104,425 | Commercial |
| Seychelles | 98,347 | Commercial |
| Antigua and Barbuda | 97,929 | Commercial |
| Isle of Man | 85,033 | Commercial |
| Andorra | 77,265 | Commercial |
| Dominica | 71,986 | Commercial |
| Cayman Islands | 65,722 | Commercial |
| Bermuda | 62,278 | Commercial |
| Marshall Islands | 59,190 | Commercial |
| Northern Mariana Islands | 57,559 | Commercial |
| Greenland | 56,770 | Commercial |
| American Samoa | 55,191 | Commercial |
| Saint Kitts & Nevis | 53,199 | Commercial |
| Faeroe Islands | 48,863 | Commercial |
| Sint Maarten | 42,876 | Commercial |
| Monaco | 39,242 | Commercial |
| Turks and Caicos | 38,717 | Commercial |
| Saint Martin | 38,666 | Commercial |
| Liechtenstein | 38,128 | Commercial |
| San Marino | 33,931 | Commercial |
| Gibraltar | 33,691 | Commercial |
| British Virgin Islands | 30,231 | Commercial |
| Caribbean Netherlands | 26,223 | Commercial |
| Palau | 18,094 | Commercial |
| Cook Islands | 17,564 | Commercial |
| Anguilla | 15,003 | Commercial |
| Tuvalu | 11,792 | Commercial |
| Wallis & Futuna | 11,239 | Commercial |
| Nauru | 10,824 | Commercial |
| Saint Barthelemy | 9,877 | Commercial |
| Saint Helena | 6,077 | Commercial |
| Saint Pierre & Miquelon | 5,794 | Commercial |
| Montserrat | 4,992 | Commercial |
| Falkland Islands | 3,480 | Commercial |
| Niue | 1,626 | Commercial |
| Tokelau | 1,357 | Commercial |
| Holy See | 801 | Commercial |
