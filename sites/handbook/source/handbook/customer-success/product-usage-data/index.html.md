
---
layout: handbook-page-toc
title: "Product Usage Data Overview"
description: "The visiona and overview for how to apply product data for Sales and Customer Success teams to support their customers' top initiatives and business objectives."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

---


## Vision

To align to our GitLab values of Efficiency, Results, Transparency, and Iteration, Product Usage Data is intended for health scoring and decision making:

1. **Customer Health**: Analyze and understand the customer's overall health through user engagement and depth of deployment
1. **License Utilization**: Assist the customer in determining how well utilized licenses are, how they could be used, and assess their future needs
1. **Customer Outcomes**: Track the customer's top-level objectives to usage data
1. **Use Case Adoption**: Understand which product use cases the customer has adopted, which ones are in progress, and how to assist the customer with their goals

These outcomes will lead to greater retention and customer growth.

Product Usage Data will be accessible in:

1. Short-term: Our GTM systems (Salesforce, Gainsight, and Marketo) to enable effective outcomes by supporting the customer where _they_ are at in their customer journey
1. Short-term: GitLab accessible dashboards (Sisense) for high level overviews and trend analyses
1. Long-term: The GitLab product to enable customers to self-serve and develop their own analytics — this will be accomplished by the Product Analytics (Product) team's initiatives


## Reference documentation

* [Using Product Usage Data in Gainsight](https://about.gitlab.com/handbook/customer-success/product-usage-data/using-product-usage-data-in-gainsight/)
* [Customer Use Case adoption](https://about.gitlab.com/handbook/customer-success/product-usage-data/use-case-adoption/)
* [Metrics Dictionary](https://docs.gitlab.com/ee/development/usage_ping/dictionary.html)
* [Adoption Explorer - GitLab Documentation](https://docs.google.com/document/d/1TvSCT_yj73AS0PuLxPonuF5QHWyM3dqG_i8H1U1cwf0/edit)
* [SaaS-Activity Dashboard](https://app.periscopedata.com/app/gitlab/684495/WIP---SaaS-Activity)
* [High-Level Visual of GitLab Adoption Journey](/handbook/customer-success/vision/#high-level-visual-of-gitlab-adoption-journey)
* [Product Intelligence Overview](https://about.gitlab.com/handbook/product/product-intelligence-guide/) (details on product usage data)
  


