---
layout: markdown_page
title: "Talent Acquisition Alignment"
description: "This page is an overview of the search team alignment and the talent acquisition platform directly responsible individual in talent acquisition operations and talent brand."
---

## Search Team Alignment by Department

| Department                    | Recruiter       | Sourcer     |
|--------------------------|-----------------|-----------------|
| Executive          | Rich Kahn   | Chriz Cruz |
| Enterprise Sales, NA | Marcus Carter |  Susan Hill |
| Enterprise Sales, EMEA | Debbie Harris |  Kanwal Matharu |
| Enterprise Sales, APAC | Debbie Harris |  Kanwal Matharu |
| Commercial Sales,	Global | Marcus Carter | Susan Hill  |
| Channel Sales, NA | Steph Sarff |  J.D. Alex (interim Kanwal Matharu) |
| Channel Sales, EMEA | Debbie Harris |  Kanwal Matharu |
| Channel Sales, APAC | Debbie Harris |  Kanwal Matharu |
| Field Operations,	NA | Steph Sarff | J.D. Alex (interim Loredana Iluca) |
| Field Operations,	EMEA | Debbie Harris | Kanwal Matharu |
| Field Operations,	APAC | Debbie Harris |  Kanwal Matharu |
| Customer Success, NA | Steph Sarff | J.D. Alex (interim Kanwal Matharu) |
| Customer Success, EMEA | Debbie Harris  | Kanwal Matharu |
| Customer Success, APAC | Debbie Harris  | Kanwal Matharu |
| Marketing, Global | Steph Sarff   | J.D. Alex |
| G&A | Maria Gore | Loredana Iluca |
| Development | Rupert Douglas | Zsuzsanna Kovacs / Joanna Michniewicz  |
| Quality | Rupert Douglas   | Zsuzsanna Kovacs |
| UX  | Rupert Douglas   | Zsuzsanna Kovacs  |
| Support | Rupert Douglas (AMER, EMEA) / Matt Allen (APAC)  |  Joanna Michniewicz (AMER, EMEA) / Zsuzsanna Kovacs (APAC)  |
| Security | Matt Allen  |  Zsuzsanna Kovacs |
| Infrastructure   | Matt Allen  | Joanna Michniewicz |
| Product Management  | Matt Allen | Chris Cruz |

The Candidate Experience Specialist team operates off of a queue style system utilizing a [GitLab Service Desk](/product/service-desk/).

## Talent Acquisition Platform Directly Responsible Individual

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Comparably | Admin  | Betsy Church and Erich Wegscheider |
| Comparably | Content Management | Betsy Church |
| Comparably | Reporting | Erich Wegscheider |
| Glassdoor | Admin  | Betsy Church and Erich Wegscheider |
| Glassdoor | Responding to Reviews  | Betsy Church |
| Glassdoor | Content Management | Betsy Church |
| Glassdoor | Reporting | Erich Wegscheider |
| LinkedIn | Admin - Recruiter  | Erich Wegscheider |
| LinkedIn | Seats | Erich Wegscheider |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Talent Acquisition | Betsy Church |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Betsy Church |
| New Platform(s) | Requests | @domain |
| Recruitment Marketing  | Requests | @domain |
