---
layout: handbook-page-toc
title: "Channel Partner Training, Certifications, and Enablement"
description: "To support and scale GitLab’s continued growth and success, the  Enablement Team is developing a learning program that includes functional, soft skills, and technical training for channel and alliances partners"
---

## Partner Training, Certifications, and Enablement  
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

---

## Training, Certifications and Enablement

GitLab has created a robust training and certification program to enable partners in our ecosystem to sell, market, service and support GitLab prospects and customers.  Training materials and learning paths have been developed to support different partner roles:



*   Sales Professionals
*   Technical Sales Professionals
*   Technical and Consulting Services Professionals 

**Why get certified?**

<img src="/images/partnerenablement/doc-pencil.png" width="150" alt="" title="GitLab Certifications">    <img src="/images/partnerenablement/illustration_announcement.png" width="175" alt="" title="GitLab Certifications">

**Validate your skills, knowledge and receive recognition for your individual mastery in GitLab!**

Training courses, certification exams and other enablement materials can be accessed on the GtlLabPartner Portal (hyperlink).

### Training and Certifications for Sales Professionals

#### GitLab Sales Core
This is the basic certification for sales and the prerequisite for pre-sales technical professionals. The curriculum provides an overview of the market GitLab services, customer personas and needs, GitLab solutions and positioning of GitLab. 

By completing the GitLab Sales Core and scoring above 80% on the tests, you’ll earn a GitLab Verified Core Sales completion badge and contribute to the Sales certification program requirements for your company. Formal individual sales certifications will be available by early 2021.

<details>
<summary markdown='span'>
  GitLab Sales Core
</summary>

<b>Partner Tools and Program:</b>

<ul>
<li>Partner Portal</li>
<li>Deal Registration</li>
<li>You’ve Got Issues</li>
</ul>

<b>DevOps Technology Landscape:</b>

<ul>
<li>The Software Development LifeCycle</li>
<li>Reduce Cycle Time</li>
<li>Increase Operational Efficiencies</li>
<li>A Seismic Shift in Application Security</li>
<li>Manage Your Toolchain Before it manages you</li>
<li>QUIZ:  DevOps Technology Landscape</li>
</ul>

<b>Introduction to GitLab:</b>

<ul>
<li>What is GitLab Video</li>
<li>GitLab Infomercial</li>
<li>Toolchain White Paper</li>
<li>GitLab Value Framework</li>
<li>QUIZ: Introduction To GitLab</li>
</ul>

<b>Competitive Advantages & Strategy:</b>

<ul>
<li>Competitor Overview</li>
<li>GitLab vs. GitHub</li>
<li>QUIZ: Competition</li>
</ul>

<b>GitLab Portfolio:</b>

<ul>
<li>GitLab Company Presentation</li>
<li>Feature Comparison</li>
<li>Why Sell Ultimate / Gold</li>
<li>Pricing</li>
<li>Use Cases</li>
<li>OPTIONAL Selling GitLab Professional Services</li>
<li>OPTIONAL Plan and Manage Work with GitLab</li>
<li>QUIZ:  GitLab Portfolio</li>
</ul>

<b>GitLab Customers and Personas:</b>

<ul>
<li>GitLab Value - Discovery Guide</li>
<li>Customer Success Stories & Proof Points</li>
<li>DevOps Director Buyer Persona</li>
<li>OPTIONAL - Learn about Other Buyer Personas</li>
<li>QUIZ: GitLab Customers</li>
</ul>

</details>

<img src="/images/partnerenablement/gitlab-verified-sales-core-personal.png" width="150" alt="" title="GitLab Verified Sales Core Partner">


### Training and Certifications for Technical Sales Professionals (Solution Architects / Sales Engineers)

#### GitLab Solution Architect Core
This is the basic certification for pre-sales technical professionals and provides a deeper understanding of demonstrating, deploying, integrating and optimizing GitLab solutions. This path is a mix of online learning and hands-on labs. GitLab Sales Core is the prerequisite for GitLab Solution Architect Core. GitLab Solution Architect Core meets the program requirement for Pre-sales technical certification, and individuals will recieve a GitLab Core Solution Architecture completion badge. NOTE: The hands-on lab components for this certification are not yet available for partners. Partners will be notified once it is available and will be able to earn a certification at that time.

<details>
  <summary markdown='span'>
  GitLab Solution Architect Core
</summary>

<b>GitLab Integrations:</b>

<ul>
<li>GitLab Integrations Overview</li>
<li>Jira Integration</li>
<li>Jenkins Integration Overview</li>
<li>Migrating from Jenkins to GitLab</li>
<li>GitLab Workflow with Jira issues and Jenkins Pipelines</li>
<li>Github Integration</li>
<li>GitLab as OAuth2 Authentication Service Provider</li>
<li>QUIZ: Integrations</li>
</ul>

<b>Technical Deep Dive:</b>

<ul>
<li>Auto DevOps</li>
<li>GitLab API</li>
<li>GitLab for Agile</li>
<li>GitLab Runners</li>
<li>GitLab High Availability (HA) and GitLab GEO</li>
<li>QUIZ: Technical Deep Dive</li>
</ul>

</details>

<img src="/images/partnerenablement/gitlab-verified-sales-architect-personal.png" width="150" alt="" title="GitLab Verified Solution Architect Core Partner">

#### GitLab Technical Core
GitLab Technical Core is the foundational technical training for post-sales technical professionals and can also be a valuable training for pre-sales technical professionals. As we continue to develop our services enablement offerings, partners will need to develop a custom plan with their Channel Account Managers to meet the certification requirements.

<details>
  <summary markdown='span'>
  GitLab Technical Core
</summary>

<b>GitLab Technical Fundamentals</b>

<ul>
<li>Using GitLab as a Project Management Tool</li>
<li>GitLab Basics for Developers</li>
<li>Getting Started with GitLab CI/CD</li>
<li>QUIZ: GitLab Technical Fundamentals</li>
</ul>

</details>


### Training and Certifications for Technical and Consulting Services Professionals

**Training and Certifications**

GitLab offers Sales and technical certifications that are designed to ensure Partners have the ability to apply GitLab core competencies in the field and within their own practices.  To earn a certification, candidates must first complete all relevant course material and then pass the assessment test with a score of 80% or greater. 

<details>
  <summary markdown='span'>
  GitLab Certified  Professional Services Engineer (PSE) Certification
  </summary>

<b>GitLab Partner Bootcamp</b>

<ul>
<li>Lesson 1: Code Management and Version Control</li>
<li>Lesson 2: Continuous Integration and AutoDevOps</li>
<li>Lesson 3: Custom CI Pipelines</li>
<li>Lesson 4: GitOps</li>
<li>Lesson 5: Application Security</li>
<li>Lesson 6: Value Stream Management</li>
<li>Lesson 7: Installation and Architecture</li>
<li>Lesson 8: Integrating and Extending</li>
<li>Lesson 9: Continuous Integration with Kubernetes</li>
<li>Lesson 10: Runner Configuration</li>
</ul>

</details>

**GitLab Professional Services Engineer (PSE) Certification Exam**

Individuals who earn the GitLab Professional Services Engineer (Partner) certification are able to demonstrate hands-on proficiency implementing GitLab, and are able to articulate the most common GitLab customer use cases.

<img src="/images/partnerenablement/gitlab-certified-professional-services-engineer-personal.png" width="150" alt="" title="GitLab Certified Professional Services Engineer">

Earning Criteria: To earn this certification, candidates must receive a passing score from the November 2020 GitLab Partner Bootcamp examination. The course material can be found on the [Partner Bootcamp Handbook page ](/handbook/resellers/bootcamp/) as well as the [GitLab Partner Portal ](https://partners.gitlab.com/English/)




#### GitLab Certified Trainer

We are currently developing a program that will enable select partners (by invitation) to become GitLab Certified Trainers.  Please refer back to this handbook page for updates.

You can find more about our existing [Train the Trainer program ] (/services/education/train-the-trainer/) at our Handbook Page. 



#### Training and Certifications Roadmap FY22

GitLab Partner Sales Professional Certification **Q1 FY22**

<img src="/images/partnerenablement/gitlab-certified-sales-professional-personal.png" width="150" alt="" title="GitLab Certified Sales Professional">

Individuals who have completed this certification are able to articulate GitLab solutions, ideal customer personas and needs, industry relevant insights, customer environments where opportunities for GitLab are most relevant, along with GitLab Advanced Use Case positioning.

Earning Criteria: Completion of the GitLab Sales Core foundational training plus a minimum of 2 (two) expanded Use Case trainings and score above 80% on the knowledge assessments.


GitLab Partner Solutions Architect (SA) Certification **Q1 FY22**

<img src="/images/partnerenablement/gitlab-certified-solutions-architect-personal.png" width="150" alt="" title="GitLab Certified Solution Architect">



#### Certification Award Process

To sign up for a course, please visit the Partner Portal (hyperlink). Upon successful completion of each learning path, individuals will be awarded a Badge of Completion and / or a Certificate. Badges and certifications will be delivered electronically via email, and will be granted no later than 10 business days after completion. 




### Additional Enablement Resources

*   Webcasts - GitLab hosts a monthly webinar the first Thursday of each month,  providing deep dive learning on key GitLab topics.  You can access Partner Webcast Archives in the Asset Library on the [GitLab Partner Portal. ](https://partners.gitlab.com/English/)
*   Technical - Pre-Sales, Services Engineers and Solution Architects can join the Channel SA’s for a monthly Tech Chat. Use this** **[Partner Tech Chat](https://gitlab.com/gitlab-com/partners/tech-chats)** **board to submit future topics and questions. 
*   GitLab Partners’ Marketing team members are invited to join our monthly Partner Marketing Webcasts. Learn about the latest Partner marketing campaigns, resources and more. 
*   The [GitLab Partner Portal ](https://partners.gitlab.com/English/)provides Partners with easy access to additional sales resources, webcast replays, competitive analysis, event calendar, Marketing campaigns, support and more. 
*   Newsletter - sign up [here ](https://gitlab.us19.list-manage.com/subscribe?u=5a5f55e4e0f03037d96416766&id=2321e18463)
*   [GitLab Virtual Commit ](https://about.gitlab.com/events/commit/) - Commit 2020 was a 24-hour virtual experience filled with practical DevOps strategies shared by leaders in development, operations, and security. You’ll discover innovative solutions to help you solve your industry's greatest challenges, including enhancing culture and shortening release times.
*   [GitLab Handbook Resources ](/handbook/resellers/) Start your handbook search on the Channel Partner Handbook Page 
