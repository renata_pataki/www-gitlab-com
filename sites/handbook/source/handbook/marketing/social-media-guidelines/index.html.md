---
layout: handbook-page-toc
title: Team Member Social Media Policy and Guidelines
description: This is the GitLab employee social media policy
twitter_image: 
twitter_image_alt: GitLab Social Media Policy and Guidelines page
twitter_site: 'gitlab'
twitter_creator: 'gitlab'

---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction
{:.no_toc}

Thanks for checking out the GitLab Team Member Social Media Policy and helpful Guidelines! We have two goals:
1. Empower you to talk about GitLab and your work on your own social media channels by providing best practices guidelines. 
2. Detail the limitations of transparency and disclosure in a public organization to hold ourselves and each other accountable for the social media policies we've agreed to during our onboarding. 

Be diligent, and if it's questionable, don't say it. You are personally responsible for the social posts, likes and shares, and replies you post on social media while representing GitLab. Everything you publish is publicly viewable and will be available for a long time, even if redacted. Remember that you represent GitLab and our culture. When commenting on posts, please keep in mind: "Don't argue but represent".

## Social Media Policy for Team Members

You'll be asked to confirm reading this section during your onboarding to the company. We'll ask everyone to review and sign off on review in partnership with legal for existing employees.

The GitLab Social Media policy for team members applies to traditional social media channels, like Twitter and LinkedIn, as well as "social-like" sites, like HackerNews, Reddit, blog comments (on the GitLab website, Medium, or any other blog site), message boards, and forums (including the GitLab Forum). Any website where you can share a post or comment on a post would be considered a social media website for the purposes of this policy.

##### As business needs change and GitLab continues on our journey, this policy will be updated. We'll be as transparent as possible when communicating changes, what those changes are, and when they are expected to go into effect.
{:.no_toc}

### GitLab Community Code of Conduct applies to team members as a part of the community
{:.no_toc}

Please adhere to the [Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/), as we would require all members of the community.

##### Social Media sometimes generates press and media attention or legal questions. Please refer all inquires to the Communications Team in the #external-comms Slack channel.
{:.no_toc}

### What you should do
<details>
  <summary markdown='span'>
   Do disclose that you're a team member
  </summary>
If you talk about work-related matters within your job responsibility area, you must disclose your affiliation with GitLab. If you have a vested interest in something you're posting on social, point it out. You can identify yourself as a team member in your profile bio, list GitLab as your employer, or mention that you're a team member in the post itself. Consider adding #LifeAtGitLab to the end of your social post to sum it up quickly and easily.
</details>

<details>
  <summary markdown='span'>
    Do state that it's your opinion
  </summary>
When commenting <b>on</b> the business, you're not commenting <b>for</b> the business. Unless authorized to speak on behalf of GitLab and through comms training. However, don't add your opinion to content from a news source that includes financial figures or forecasts. If you would like to share content that includes financial figures or a forecast, please only share it without adding your commentary. Not sure if you should share? Ask in the #is_this_public Slack channel.
</details>

<details>
  <summary markdown='span'>
    Do protect yourself
  </summary>
Consider what information you're sharing online and how it can be used to identify you. Everyone has a different level of privacy they want to abide by, have you thought about yours? Some of the things folks opt-out of to protect their privacy include: not tagging specific locations in posts while you're still on-site, not including phone numbers or email addresses in plain text because bots sweep for contact info, and not showing the faces of their children on public channels. You aren't required to follow any of these specifics, but consider what kind of information will be available to the public when publishing on your social media profiles.
</details>

<details>
  <summary markdown='span'>
    Do act responsibly and ethically
  </summary>
Don't misrepresent yourself. If you're not a director in your role, don't say you are. Don't imply that you're a member of a team if you're not. Share your thoughts, but disclose your role. Furthermore, <a href="https://about.gitlab.com/community/contribute/code-of-conduct/">our team members are bound by the same code of conduct we provide to the community</a>. 
</details>

<details>
  <summary markdown='span'>
    Do try to live our values
  </summary>
Easier said than done, but consider our values when engaging online. The same space and respect we grant each other is needed with strangers on social media.
</details>

### It's your job to protect GitLab, our customers, and our team members 
Whether you're an intern or our CEO, protecting the GitLab brand, the company, and our entire team is a part of your job description.

##### You can protect GitLab by not sharing the following data with the public on your social media profiles.

<details>
  <summary markdown='span'>
    Don't speculate or add forward-looking statements
  </summary>
Forward-looking statements predict, project, or use future events as expectations or possibilities. Team Members mustn't speculate or add forward-looking statements to their opinions written on social media. Forward-looking statements could include company valuation, details on customer volume, subscription growth, and more. This would include forecasting, or when data is presented to argue for or against future trends. Sometimes an article from a news source will use speculative information in their story. While this is okay to share, it is not okay to share your commentary with speculative information. Not sure if it's public? Ask in the #is_this_public Slack channel.
</details>

<details>
  <summary markdown='span'>
    Don't share customer or Team Member personal information
  </summary>
Never share personal information about our customers or fellow team members. If you'd like to share info about a customer, ask about public disclosure in the #is_this_public Slack channel. For team members, allow them to own the decision on what is and isn't public about them.
</details>

<details>
  <summary markdown='span'>
    Don't bash competitors
  </summary>
While sharing your opinion is a critical part of being authentic on social media, it's equally important to limit overly negative responses to competitor posts. We don't like trolls, and neither do our competitors. Providing constructive feedback is read very differently than trolling. Trolling occurs when you respond to social media posts with intentionally provocative or offensive messages. 
</details>

<details>
  <summary markdown='span'>
    Don't share legal information
  </summary>
Never share anything to do with a legal issue, legal case, or attorneys without first checking with legal in the #legal Slack channel. Note, that if you're looking to connect on whether or not you should publicly disclose legal issues, the answer is probably a no.
</details>

<details>
  <summary markdown='span'>
    Don't share confidential information
  </summary>
Don't share information that is considered confidential. If it's related to work in an issue, consider reviewing whether or not the issue was made confidential. If you're not sure, reach out to the #is_this_public Slack channel.
</details>

<details>
  <summary markdown='span'>
    Don't use or manipulate logos without permission from the copyright holder
  </summary>
While GitLab generally allows (and encourages) our team to use our Tanuki logo, it's not the same thing when you use a 3rd party logo. Logos from our competitors, partners, and others cannot be used or manipulated without the copyright holder's explicit permission. If you're not sure what's permissible, reach out to the #is_this_public Slack channel.
</details>

**Sharing information that would be considered confidential or a part of legal situations could negatively impact your team member status. Depending on the severity of the information, be regarded as a legal issue itself.**

### Examples of what you should, and are encouraged, to share on social media
- GitLab [blogs](https://about.gitlab.com/blog/) and [press releases](https://about.gitlab.com/press/releases/)
- Positive news media that mentions GitLab
- Industry reports that are publicly available and are ungated
- Videos from either our [brand](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg) or [Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) YouTube Channels
- Third-party articles that don’t bash competitors (mentioning them or comparing GitLab and competitors is fine)
- Retweeting or sharing GitLab brand social channel posts from [Twitter](https://twitter.com/gitlab), [LinkedIn](https://www.linkedin.com/company/gitlab-com/), [Facebook](https://www.facebook.com/gitlab/), or [Instagram](https://www.instagram.com/gitlab/)
- All of the above from our eco and alliance partners
- Items that are not connected to your job or GitLab at all! Social Media is best when you bring your true-self to the mix. DevOps isn't the only thing you're interested in, so consider posting about other passions and topics.

### Other policies

#### Connecting with Team Members on Social Media

In case you want to connect with fellow team members of GitLab on social media, you have to keep it professional. With this communication, we would like you to consider GitLab’s [Communication Guidelines](/handbook/communication/) at all times. Aligned with our [Anti-Harassment Policy](/handbook/anti-harassment/), it is expected that everyone will contribute to an inclusive and collaborative working environment and respect each other at all times.

#### Mimicking the brand or unauthorized social media accounts

Please keep your identity clear on your social media channels by not using the GitLab logo as your profile image, not adding GitLab to your @handle, and not adding the company to your display names. It should be clear to everyone that you are a team member of GitLab, but not GitLab the company. Use common sense when selecting pictures and names to use. We'll always work to get profile names and visuals updated to reflect who runs the account, but if we come across profiles that don't respond to these requests, we will report them for impersonating our brand. 

As a Team Member of GitLab, you aren't authorized to create company/brand social media profiles to use for your work. If promoting content should come from the company, you'll need to [open a request issue with the organic social team](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/admin/#requesting-social-promotion-). If the corporate marketing team encounters unauthorized profiles, they will be treated as external threats and reported for impersonation. There is no formal method for requesting new brand channels, nor is there an outline for managing to do so. It's best to use personally identified social media profiles to share your posts.

#### Contests or sweepstakes on your social media profiles

As part of your role at GitLab, you may be responsible for a contest or a sweepstakes with social media elements. It's important to follow legal guidelines. Essentially, as a representative of GitLab, if you're promoting the contest on your social media channels, it will need to follow the same rules as what the GitLab brand channels will need to follow. You can [learn more about legal and contests in the handbook here](https://about.gitlab.com/handbook/legal/marketing-collaboration/#contests-and-sweepstakes).

## Ramifications of not following the social media policy

Suppose we find any statements or claims that are false or misleading, or we discover an activity that does not follow the policy. In that case, we will contact you to correct the situation. 

Suppose you are consistently disregarding social media policy, especially if you fail to disclose your relationship to GitLab, make false or misleading statements about GitLab or our products and services. In that case, we may discontinue our relationship with you. 

## Transform this policy into action with a social media training and certification

Take this policy and turn it into a social media certification in less than 45 minutes with the [social media 101 training and certification](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications-resourses-trainings/#social-media-101-training--certification). You'll find details on the training and what to expect [here](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications-resourses-trainings/#social-media-101-training--certification).

##### This is the end of the GitLab Team Member Social Media Policy. You'll find guidelines along with some best practices below.
{:.no_toc}

---------

## Team member social media guidelines

If you've reviewed the policies above, you're ready to start using social media. We've included some best practices and what to avoid below. Don't hesitate to reach out to the social media team for any assistance or questions in the #social_media_action Slack channel.

### You ought to promote your public-facing work

If you've written a blog for our site, contributed to our latest release, or joined a webinar/webcast, you should want to tell your networks about it. Not only does this provide a way to build your following and expertise in the public domain, but it's also a great way to add critical promotion to your work. Promoting on social media isn't just about the GitLab brand channels. It's an orchestra of efforts, which includes team member support and advocacy.

### Use your voice

When responding to posts from your account, feel free to incorporate your style and voice. Talk to people as if you were talking to them in person. Be sure to speak with “we” and not "I" (as often as appropriate) to represent the company and the community.

Someone doesn’t like something? Ask them to tell us more in the issue tracker. Someone thinks GitLab could be better? Invite them to submit a feature proposal. Any criticism is an opportunity to improve in our next iteration.

Please do not engage in competitor bashing. Instead, highlight positive differences — it's best to focus on the ways that GitLab outperforms other solutions.

### Dealing with conflict and avoiding arguments

You may come across angry users from time to time. When dealing with confrontational people, it’s important to remain level-headed. Sometimes, the best course of action is to walk away and not engage with the person at all. Use your judgment in how you approach rude or off-putting comments from strangers in real life to help you decide.

For a foundational understanding of these nuances, read [GitLab's guide to communicating effectively and responsibly through text](/company/culture/all-remote/effective-communication/).

- Assume good faith. People have opinions and sometimes they’re strong ones. It’s usually not personal.
- If it’s getting personal, step away from the conversation and delegate to someone else.
- Sometimes all people need is acknowledgment. Saying “Sorry things aren’t working for you” can go a long way.
- **You’re allowed to disagree with people.** Try to inform the person (respectfully) why they might be misguided and provide any links they may need to make a more informed decision. Don’t say “you’re wrong” or “that’s stupid.” Instead try to say “I disagree because…” or “I don’t think that’s accurate because…”.
- You're the boss of your social channels, and you can choose not to engage. _Consider not engaging with that person and ignoring their comments. The probability of "Twitter arguments" around various crises and controversies is high, and it is recommended that you don't fall into a fight with someone on the internet._

**If you are unsure if you should respond to someone who has responded to your posts, join the #social_media_action Slack channel and ask for feedback.**

### Tips for writing your own social media posts

<details>
  <summary markdown='span'>
    Twitter Copy Length
  </summary>

If your copy is on the longer side, try to visually break it into paragraphs or one-liners, even using emojis/bullets. Consider composing a thread if you have something longer than 280 characters, like an anecdote or a schedule. <a href="https://help.twitter.com/en/using-twitter/create-a-thread">Learn how to make a Twitter Thread here</a>.
</details>

<details>
  <summary markdown='span'>
    LinkedIn Copy Length
  </summary>

You aren't bound by character count the same way you are on Twitter, so you can think about LinkedIn posts as mini blog posts. This is especially effective if you have a perfect anecdote that you think might complement the asset you're promoting. However, keep in mind that, unless it's <i>really</i> interesting, people won't click `more` on your post (the only way to read an entire post longer than a few lines), so keep it on the short side unless you have a great hook above the `more` button.

Longer posts are easier to read if you have many paragraph breaks, so feel free to be creative with your formatting: one-liners, emojis, bullet points, etc., can help break up your text visually.
</details>

<details>
  <summary markdown='span'>
    Tone of voice
  </summary>

Tone check your copy. The most important thing is that it sounds like you, a human being. Overly contrived posts generally do not do well. If loading up your post with emojis isn't your style, don't force it.
</details>

<details>
  <summary markdown='span'>
    Visuals
  </summary>

Add an image or video! This makes a massive difference for impressions (and clicks), so it's worth the hassle. Feel free to experiment if you have an idea to personalize it in a relevant way.
</details>

<details>
  <summary markdown='span'>
    Style
  </summary>

Don't worry about writing posts that sound like news or have an editorial perspective; the brand channels will have that covered. It would be best to personalize the message if you wrote like you're learning/loving/doing something new and fun.

Instead of sharing a quote from a speaker, consider sharing the quote and adding why it personally resonated with you.
</details>

<details>
  <summary markdown='span'>
    Always use trackable links when available
  </summary>

To measure the traffic you're driving, it is paramount that you use a tracked, UTM link. If the social team is providing an enablement issue, we'll have the tracking embedded already. If you retweet GitLab posts, you're already safe. Tracked links aren't generally available to everyone. However, if you plan on using your social channels as a part of your role at GitLab, reach out to the social team in the #social_media_action Slack channel, and we can set you up with a tracking sheet and teach you how to use it.
</details>

### Profile assets (to be updated)

Profile assets for social media can be found [in the corporate marketing repository](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/social-media/profile-assets/png/assets/_general)

**Please do not use the GitLab logo as the avatar for your accounts on social. You are welcome to use our branded banners, but your profile avatar mustn't lead users to confuse your account with the official GitLab accounts.**

While you should display the fact that you work at GitLab in your bio if you intend to advocate for GitLab on social, we suggest that you avoid including the word `GitLab` in your handle. Team member advocacy is incredibly valuable, and we are lucky to have so many engaged team members, but creating an account to _solely_ post about GitLab is not adequate. Team member advocacy is so powerful that people [trust employees](https://www.scribd.com/doc/295815519/2016-Edelman-Trust-Barometer-Executive-Summary) more than brands and executives. Your advocacy is powerful when it is authentic, and having an account that only exists to promote GitLab will not ring true to others who browse your tweets.

## FAQ

<details>
  <summary markdown='span'>
    Am I required to share/like a post from GitLab social channels?
  </summary>

<i>No, you're not required to participate in any way.</i>
</details>

<details>
  <summary markdown='span'>
    Am I required to have the social or comms team review my posts before I publish them?
  </summary>

<i>No, the GitLab social team will never require a review or another oversight of your own personal social media posts.</i>
</details>

<details>
  <summary markdown='span'>
    Am I required to identify myself as a GitLab employee on my social media channel?
  </summary>

<i>No, you are not required to identify yourself as an employee for simply having a personal channel. However, if you are going to engage with community members or be considered an authority in your space, you ought to be transparent and identify yourself as a team member.</i>
</details>

<details>
  <summary markdown='span'>
    I don't want to write my own posts. I'd rather share GitLab's social posts. Is there an easy way to know when they are published?
  </summary>

<i>Yes, you can join the `#social_media_posts` Slack channel for easy access to our latest posts as they publish.</i>
</details>

<details>
  <summary markdown='span'>
    I don't know how to respond to a comment someone posted to me. How can I find help?
  </summary>

<i>Consider joining the `#social_media_action` Slack channel for an easy way to connect with the social team for help.</i>
</details>

<details>
  <summary markdown='span'>
    What should I do if a friend/follower/user responds to me about controversies?
  </summary>

<i>Consider not engaging with that person and ignoring their comments. The probability of "Twitter arguments" around various crises and controversies is high and it is recommended that you don't fall into a fight with someone on the internet.</i>
</details>

<details>
  <summary markdown='span'>
    Where can I learn more about social media for our company?
  </summary>

<i>You can <a href="https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/">check out our social media handbook here</a>.</i>
</details>
