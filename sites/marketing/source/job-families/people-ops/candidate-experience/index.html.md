---
layout: job_family_page
title: "Candidate Experience"
---

## Candidate Experience Specialist

The GitLab Candidate Experience Specialists work to create positive experiences for GitLab candidates and hiring teams. In a growing fast-paced environment, the Candidate Experience Specialist is a dynamic team member who helps create an amazing candidate experience, improve our existing hiring practices, and deliver exceptional results to our hiring teams. GitLab strives to be a preferred employer and relies on the Candidate Experience Specialists to act as brand ambassadors by embodying the company values during every interaction with potential team members. The Candidate Experience Specialist is enthusiastic about working with high volume and is dedicated to helping GitLab build a qualified, diverse, and motivated team. The Candidate Experience Specialist reports to the Team Lead, Candidate Experience.

### Job Grade

The Candidate Experience Specialist is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Collaborate with Recruiters, hiring teams and Recruiting leadership to understand and implement the hiring plan for each job requisition
* Coordinate with the Recruiters to maintain correct process information in the Applicant Tracking System (ATS) and GitLab repositories
* Build and update an effective network of internal and external resources to call on as needed
* Ensure candidates receive timely, thoughtful and engaging messaging throughout the hiring process
* Work in Applicant Tracking System (ATS) to help recruiters maintain a positive candidate experience for candidates
* Helps manage candidate traffic for all roles
* Act as the [Recruiting Coordinator](https://about.gitlab.com/handbook/hiring/recruiting-framework/coordinator/) for the interview process
* Assist with conducting reference checks, if applicable, for all candidates entering the final round of the interview phase
* Create and stage employment contracts for candidates receiving an offer of employment on behalf of the hiring manager and recruiter
* Work to create a seamless handoff of new team members to the [People Operations Specialist](https://about.gitlab.com/job-families/people-ops/people-operations/) team
* Promote our values, culture and remote only passion

### Requirements

* Desire to learn about Talent Acquisition and GitLab from the ground level
* Demonstrated ability to work in a team environment and work with C-Suite level candidates with a focus on delivering an excellent candidate experience
* Ambitious, efficient and stable under tight deadlines and competing priorities
* Proficient in Google Workspace
* Ability to build relationships with team members, hiring managers and colleagues across multiple disciplines and timezones
* Outstanding written and verbal communication skills across all levels
* Willingness to learn and use software tools including Git and GitLab
* Organized, efficient, and proactive with a keen sense of urgency
* Successful completion of a [background check](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#background-checks)
* Excellent communication and interpersonal skills
* Prior experience using an applicant tracking system, Greenhouse experience is a plus
* Ability to recognize and appropriately handle highly sensitive and confidential information
* Experience working remotely is a plus
* Share our [values](/handbook/values/) and work in accordance with those values
* Ability to learn and use outbound talent outreach practices
* Ability to use GitLab

## Senior Candidate Experience Specialist

Senior Candidate Experience Specialist shares the same requirements and responsibilities as the Intermediate Candidate Experience Specialist with the addition of the following:

### Job Grade

The Senior Candidate Experience Specialist is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Serve as a mentor to the rest of the team through leading by example, sharing best practices, and a resource for questions and guidance
* Serve as an example of dogfooding Gitlab.com and assist team members with questions
* Provide insight and feedback for process improvement opportunities
* Lead by example and performance with the team OKR’s and Performance Indicators
* Serve as a subject matter expert
* Consistently provide a great candidate experience by teaching others how to embody the GitLab values

### Requirements

* 3-5 years experience in recruiting/HR/People
* Consistent track record and strong performance
* Experience working directly with hiring teams
* High sense of urgency
* Proven organizational skills with high attention to detail and the ability to prioritize
* Confidence to learn new technologies and translate that learning to others
* Experience as mentor, guide, or subject matter expert
* Ability to guide others on outbound talent outreach practices

## Candidate Experience Lead

The Candidate Experience Lead has deep experience in assessing organizational needs and developing a variety of solutions to drive development and growth within the team and department. They provide leadership to build and scale initiatives that focuses on recruiting culture, team development, strengthening our team members, and continual process improvement to ensure candidates receive an amazing experience.

### Job Grade

The Candidate Experience Lead is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Own a req load (approx. 50% capacity)
* Manage a team of Candidate Experience Specialists
* Provide an exceptional and high touch candidate experience
* Serve as a mentor to the rest of the recruiting team through leading by example, sharing best practices, and a resource for questions and guidance
* Provide input into the recruiting strategies
* Act as a key business partner to members of the organization to improve processes for recruiting
* Work closely with various internal functional groups to understand business requirements, and consult on talent solutions
* Stay connected to the competitive landscape, including trends in recruiting
* Ensure that the teams maintain a high level of data integrity with our ATS and other people systems
* Provide consistent training and best practices to the rest of the recruiting team

### Requirements

* At least 6 years of recruiting experience within a growing organization
* At least 2 years experience recruiting for senior level roles
* Consistent track record and strong performance reviews
* Worked closely with sourcers, recruiters, and Candidate Experience Specialist
* Experience working directly with hiring managers
* High sense of urgency
* Proven organizational skills with high attention to detail and the ability to prioritize
* Confidence to learn and translate that learning to others
* Experience and proficiency with Applicant Tracking Systems and other recruiting software (ideally including Greenhouse and LinkedIn Recruiter)
* Experience as leader, mentor, guide, or subject matter expert
* Ability to lead others on outbound talent outreach practices

## Career Ladder

The next step in the Candidate Experience job family is to move to the [Recruiting Operations](/job-families/people-ops/recruiting-operations/) job family, [Recruiter](/job-families/people-ops/recruiter/) job family or the [Sourcer](https://about.gitlab.com/job-families/people-ops/recruiting-sourcer/) job family.

## Performance Indicators

*   [Average candidate ISAT](/handbook/hiring/metrics/#interviewee-satisfaction-isat)
*   [Hires vs. Plan](/handbook/hiring/metrics/#hires-vs-plan)
*   [Time to Offer Accept](/handbook/hiring/metrics/#time-to-offer-accept-days)
*   [CES Service Desk Issues Response Time and Issue Distribution](/handbook/hiring/metrics/#ces-service-desk-metrics)
*   [CES Response Time](/handbook/hiring/metrics/#ces-service-desk-metrics)
*   [Candidate Time Per Stage](/handbook/hiring/performance_indicators/#candidate-time-per-stage)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/).

   * Qualified candidates will be invited to schedule a 30 minute screening call with a Recruiting Manager
   * Then, candidates will be invited to schedule two 30 minute interviews with two separate Peers and a 30 minute interview with another Recruiting Manager
   * Finally, candidates will be invited to a 45 minute interview with the Hiring Manager

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).
