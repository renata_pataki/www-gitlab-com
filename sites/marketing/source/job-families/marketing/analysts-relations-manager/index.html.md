---
layout: job_family_page
title: "Analyst Relations Manager"
---

## Analyst Relations Manager

As a Global Industry Analyst and Influencer Relations Manager, you will be part of a team managing all industry analyst, influencer, and thought leadership relationships for GitLab.

### Job Grade

The Analyst Relations Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Requirements

- Prior experience working with industry analysts and influencers in B2B enterprise software and services required.
- Specific, significant experience in an industry Analyst Relations role in enterprise software a plus.
- Understanding of how to build and maintain relationships with key influencers, leading industry analysts, and analyst firms.
- Prior, significant experience working with senior executives a plus.
- Prior experience negotiating contracts with Industry Analyst firms a plus.
- Excellent project and time management skills.
- Excellent written and oral communications skills.
- Self-starter with a strong sense of ownership.
- Able to prioritize in a complex, fast-paced and lean organization.
- Passion for helping build a world-class, innovative analyst relations program and desire to own and refine key operational processes
- You share our [values](/handbook/values/), and work in accordance with those values
- Ability to use GitLab

### Responsibilities

- Help design, execute, and manage GitLab's overall industry analyst, influencer, and thought leader strategy and plan.
- Create, nurture, and manage positive relationships with industry analysts, industry influencers, and thought leaders, serving as a key, centralized contact point and connector.
- Establish and maintain regular communication with analysts and influencers via multiple channels which you will develop to engage and educate these communities on GitLab strategy, roadmap and product updates.
- Ensure all analysts and influencer advice informs all applicable business activities and that analyst/influencer/thought leader publications and/or other interactions mentioning GitLab are leveraged appropriately to help build market awareness, increase lead generation effectiveness, and simplify selling.
- Plan, schedule, and manage analyst/influencer program activities such as participation in analyst research, conferences, advisory days, briefings, inquiries, and reporting.
- Drive alignment with sales, marketing and product teams to maximize program impact.
- Schedule briefings, inquiries, demos and advisory days with key analysts and influencers, GitLab customers, and GitLab executives.
- Prioritize and manage incoming analyst requests for information, research support, research review, customer references, and event speakers.
- Respond to, serve, and manage internal requests for Industry Analyst/Influencer/Thought Leader interactions, publications, research, event support, and other requests as needed.
- Negotiate, secure, and manage contracts and related services ensuring they support both strategic and tactical, near term and future requirements. Ensure that contracted services are fully utilized.
- Support the GitLab reference program, market research activities, and serve as a program manager for the GitLab Thought Leadership Advisory Board.

## Senior Analyst Relations Manager

As Analyst Relations Managers progress throughout their career at GitLab or we find the need to hire an Analyst Relations Manager who has more years of relevant experience, there will be a need for a Senior Analyst Relations Manager position in the Market Insights Organization. The Senior Analyst Relations Manager will report into the Manager, Market Insights.

### Job Grade

The Senior Analyst Relations Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Requirements

This role includes all of the requirements above, plus:

- 5+ years of specific, significant experience in an industry Analyst Relations Manager role in enterprise software.
- Proven ability to build and drive a budget and forecast spend.
- Specific experience in garnering industry analyst interest and coverage.
- Strong orientation to managing program details.

### Responsibilities

This role includes all of the responsibilities above, plus:

- Be a leader in working cross-functionally to drive the execution of analyst relations plans, aligning with other areas of marketing.
- Driving negotiations with industry analyst and related firms to consistently optimize spend.
- Be willing to act as a Senior leader on the Market Insights team, mentoring and guiding Analyst Relations Managers.
- Be a leader in building and driving process within the Market Insights team.

## Staff Analyst Relations Manager

As Senior Analyst Relations Managers progress throughout their career at GitLab or we find the need to hire a Senior Analyst Relations Manager who has more years of relevant experience, there will be a need for a Staff Analyst Relations Manager position in the Market Insights Organization. The Staff Analyst Relations Manager will report into the Manager, Market Insights.

### Job Grade

The Staff Analyst Relations Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Requirements

This role includes all of the requirements above, plus:

- Strong analytical skills and proven ability to use data to optimize program performance and inform future strategies.
- Plan and operate in a transparent manner for cross-organizational visibility and be a leader in sharing best practices with other Market Research and Customer Insights team members.
- Experience organizing industry analyst events that both support industry analyst research efforts and provide insights back into all applicable business activities.
- Experience developing internal analyst relations training programs and maintaining consistent internal and external communications of analyst relations activities.

### Responsibilities

This role includes all of the responsibilities above, plus:

- 10+ years specific, significant experience in an industry Analyst Relations role in enterprise software.
- Ability to easily transition from high level strategic thinking to creative and detailed execution.
- Excellent communicator with proven ability to clearly convey ideas and data in written and verbal presentations to a variety of audiences.
- Ability and interest to design, build, implement, and use data-driven mechanisms to simplify and speed decision making regarding analyst relations activities.

## Manager, Analyst Relations  

As Senior Analyst Relations Managers progress throughout their career at GitLab or we find the need to hire a Senior Analyst Relations Manager who has more years of relevant experience, there will be a need for a Manager, Analyst Relations position in the Market Insights Organization. The Manager, Analyst Relations will report into the Manager, Market Insights.

### Job Grade

The Manager, Analyst Relations is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Requirements

This role includes all of the requirements of a Staff Analyst Relations Manager position. 


### Responsibilities

This role includes all of the responsibilities of a Staff Analyst Relations Manager, plus:

- Recruit, coach and manage Analyst Relations team members to live our values.
- Set team direction, staff for scale and develop the skills and career paths of all team members.
- Ensure all our Analyst Relations team members are very effective.
- Promote and identify underperformance proactively.
- Own the quality, effectiveness and performance of the Analyst Relations team results.
- Measure and improve the happiness and productivity of the team.


## Career Ladder

The next step in the Analyst Relations job family is not yet defined at GitLab. 

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a 45 minute interview with a Senior Product Marketing Manager
- Then, candidates will be invited to schedule a 45 minute interview with an Analyst Relations Manager
- Candidates will then be invited to schedule a 45 minute interview with our VP of Product or a Director of Product.
- Following that, candidates will be invited to schedule a 45 minute interview with our Manager, Market Insights.
- Then, Candidates will be invited to schedule a 45 minute interview with our VP of Product Marketing.
- Finally, selected candidates may be asked to interview with our CMO and/or CEO.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
