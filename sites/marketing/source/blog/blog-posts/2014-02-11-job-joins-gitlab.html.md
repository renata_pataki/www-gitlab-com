---
title: Job van der Voort joins Gitlab as Junior Service Engineer
description: "From this week on, Job joins Gitlab.com as junior service engineer."
canonical_path: "/blog/2014/02/11/job-joins-gitlab/"
date: February 11, 2014
categories: company
---

From this week on, Job joins Gitlab.com as junior service engineer. He will be providing support to our customers, support the Gitlab open source community and help grow Gitlab.

Job became deeply passionate for software engineering while working in neuroscience and becoming increasingly frustrated with the lack of version control in science. At the end of 2012 he decided to make the jump and start to work as a software developer fulltime. He's very excited to be back in a lab that is all about version control and beautiful code.

Besides code, Job has an obsession for design and entrepreneurship and loves to play videogames of any kind. 


