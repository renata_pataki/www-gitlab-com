---
layout: markdown_page
title: "FY22-Q2 OKRs"
description: "View GitLabs Objective-Key Results for FY22 Q2. Learn more here!"
canonical_path: "/company/okrs/fy22-q2/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from May 1, 2021 to July 31,2021.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2021-03-29 | CEO shares top goals with E-group for feedback |
| -4 | 2021-04-05 | CEO pushes top goals to this page |
| -3 | 2021-04-12 | E-group propose OKRs for their functions in [Epics and Issues nested under the CEO's OKRs](/company/okrs/#executives-propose-okrs-for-their-functions). These issues and epics are shared in #okrs Slack channel|
| -2 | 2021-04-19 | E-group 50 minute draft review meeting |
| -2 | 2021-04-19 | E-group discusses with their respective teams and polish their OKRs |
| -1 | 2021-04-26 | CEO reports post links to final OKR Epics in #okrs slack channel and @ mention the CEO and CoS for approval |
| 0  | 2021-05-01 | CoS updates OKR page for current quarter to be active |


## OKRs

### 1. CEO: New delivery methods launched
[Epic 1422](https://gitlab.com/groups/gitlab-com/-/epics/1422)
1. **CEO KR:** GitLab Private generally available (GA). [Issue 11225](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11225)
1. **CEO KR:** Cloud licensing for 80% of new orders. [Issue 11226](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11226)
1. **CEO KR:** 5 'Upgrade features' available (when going from CE to EE). [Issue 11227](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11227)

### 2. CEO: SaaS Usability improvement
[Epic 1423](https://gitlab.com/groups/gitlab-com/-/epics/1423)
1. **CEO KR:** Improve SUS to 72.5. [Issue 11228](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11228)
1. **CEO KR:** 10k product expertise certifications. [Issue 11229](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11229)
1. **CEO KR:** GitLab Region in alpha (sharded DB Gitaly style). [Issue 11230](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11230)

### 3. CEO: Sustained long-term growth
[Epic 1424](https://gitlab.com/groups/gitlab-com/-/epics/1424)
1. **CEO KR:** 'DevOps Platform ambassador' marketing group reaching 100k people. [Issue 11231](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11231)
